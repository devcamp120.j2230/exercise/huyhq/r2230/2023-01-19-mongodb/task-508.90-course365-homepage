const express = require("express");                                          //thư viện express


const path = require("path");                                                //thư viện path

const app = express();

const port = 8000;

app.use(express.static(__dirname + "/view"));                               //middleware cho phép load ảnh trong view

app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/view/course365.html"))            //đường dẫn tới file
})

app.listen(port, () => {
    console.log("App on Port: " + port);
})